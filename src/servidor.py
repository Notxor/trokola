import socket
import remoto

IP = '127.0.0.1'
PORT = 6969

class Servidor(object):
    """
    Establece y guarda el socket principal y una lista de los
    clientes conectados.
    """
    def __init__(self, ip = IP, port = PORT):
        super(Servidor, self).__init__()
        self.ip = ip
        self.port = port
        self.clientes = []
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((ip, port))

    def start(self):
        self.socket.listen()
        print(f'[*] Escuchando en {self.ip}:{self.port}')
        while True:
            socket_cliente, direccion = self.socket.accept()
            print(f'[*] Conexión aceptada desde {direccion[0]}:{direccion[1]}')
            cte = remoto.Remoto(len(self.clientes)+1, self, socket_cliente, direccion)
            self.clientes.append(cte)
        self.socket.close()

    def procesar(self, cliente_id, cadena):
        print(f'[*] Recibido «{cadena}» desde [{cliente_id}]')
        for s in self.clientes:
            if s.id != cliente_id:
                resp = f'\n[{cliente_id}]: {cadena}\n> '
                s.socket.send(bytes(resp, 'utf-8'))

if __name__ == '__main__':
    servidor = Servidor()
    servidor.start()
