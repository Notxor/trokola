
class Entidad(object):
    """Todo lo que existe en el mundo virtual, es una entidad

    """
    def __init__(self, id = "", padre="", descrip = ""):
        super(Entidad, self).__init__()
        self.id = id
        self.padre = padre
        self.descrip = descrip
        self.contenido = []
