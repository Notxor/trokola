import socket
import threading
import servidor

class Remoto(object):
    """
    Datos de conexión del socket y bucle principal de comunicación.
    """
    def __init__(self, id, serv, socket, direccion):
        super(Remoto, self).__init__()
        self.id = id
        self.servidor = serv
        self.socket = socket
        self.direccion = direccion
        self.conectado = True
        hilo = threading.Thread(target=self.loop, args=(socket,))
        hilo.start()

    def loop(self, socket):
        socket.send(b'> ')
        while self.conectado:
            respuesta = socket.recv(1024).decode("utf-8").rstrip()
            if respuesta == "exit":
                socket.send(b'saliendo...\n')
                break
            self.servidor.procesar(self.id, respuesta)
            socket.send(b'> ')
        socket.close()
        self.conectado = False
