import entidad

class Mundo(object):
    """El juego que vamos a cargar debe encontrarse en un objeto «Mundo».

    """
    def __init__(self, entidades = []):
        super(Mundo, self).__init__()
        self.entidades = entidades
        
    def nuevoId(self):
        return len(self.entidades) + 1
